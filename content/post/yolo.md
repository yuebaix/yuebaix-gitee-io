---
title:       "欢迎来到月白的博客"
subtitle:    "Yolo"
description: "人生只有一次"
date:        2021-09-08T12:48:07+08:00
author:      "月白"
URL:         "/2021/09/09/yolo/"
image:       "https://z3.ax1x.com/2021/09/05/hfeZYF.png"
categories:  ["LIFE"]
tags:        ["感悟"]
---

> you only live once.
